/**
 * Our Searcher class
 * Note: $input and $results are jQuery objects
 */
function Searcher( $input, $results ) {
    this.$input = $input;
    this.$results = $results;

    this.ajax = false;
    this.query = '';

    this.searchPostsDebounced = _.debounce( this.searchPosts, 200 );
    this.$input.on( 'keyup change', _.bind( this.onQueryChanged, this ) );
}

/**
 * Callback on input text change.
 */
Searcher.prototype.onQueryChanged = function() {

    var value = this.$input.val();
    if ( this.query === value && this.query.length ) {
        return;
    } // end if

    if ( this.ajax ) {
        this.ajax.abort();
    } // end if

    if ( "" === value.trim() ) { // corrected by adding .trim() and return statement
        this.clear();
        return;
    } else {
        this.searching();
    } // end if

    this.searchPostsDebounced( value );
}

/**
 * This function searches the posts.
 */
Searcher.prototype.searchPosts = function( query ) {

    if ( this.ajax ) {
        this.ajax.abort();
    } // end if

    this.query = query;
    this.ajax = jQuery.ajax({
        url: '/wp-admin/admin-ajax.php',
        data: {
            action: 'neliops_search_posts',
            q: query
        },
        success: _.bind( this.processResult, this )
    });
};

/**
 * This function processes the AJAX result and updates the view.
 */
Searcher.prototype.processResult = function( result ) {

    if ( ! result.success ) {
        this.clear();
    } // end if

    this.draw( result.data );
};

/**
 * This function prints the list of posts in the result area.
 */
Searcher.prototype.draw = function( posts ) {

    if ( 0 === posts.length ) {
        this.$results.html( 'No posts match the search criteria.' );
        return;
    } // end if

    var list = '<ul>';

    for ( var i = 0; i < posts.length; ++i ) {

        var post = posts[i];
        list += '<li><a href="' + post.permalink + '">' + post.title + '</a></li>';

    } // end for

    list += '</ul>';
    this.$results.html( list );

};

/**
 * This function modifies the results area to notify the user we're currently searching.
 */
Searcher.prototype.searching = function() {
    this.$results.html( 'Searching&hellip;' );
}

/**
 * This function clears the result area
 */
Searcher.prototype.clear = function() {
    this.$results.empty();
}