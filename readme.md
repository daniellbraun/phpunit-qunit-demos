# PHPUnit / QUnit Wordpress Unit Testing Example

This is a wordpress plugin based on [this article](https://neliosoftware.com/blog/introduction-to-unit-testing-in-wordpress-ajax/). The point of it is to demonstrate the use of PHPUnit (unit test PHP) and QUnit (unit test JS). It is also a good demonstration of a simple AJAX based plugin that searches post titles with queries entered into a search box at the bottom of post content. It searches while you type.

## Please refer to [the Wiki](https://bitbucket.org/daniellbraun/phpunit-qunit-demos/wiki/Home) on how exactly to write the tests. [The Wiki](https://bitbucket.org/daniellbraun/phpunit-qunit-demos/wiki/Home) also contains additional examples.
This readme will only go through the basics of getting things setup in our environment.

## Installation and Setup of QUnit
QUnit can be made availabe to your scripts a number of ways. The simplest (as used in the plugin in this repo) is to simply include it with the CDN url. Please refer to the [offical QUnit homepage](http://qunitjs.com/) for options including the CDN link, NPM and Bower.

The specific implementation used in this plugin is outlined in [this article](https://neliosoftware.com/blog/introduction-to-unit-testing-in-wordpress-ajax/) and in [the Wiki](https://bitbucket.org/daniellbraun/phpunit-qunit-demos/wiki/QUnit%20-%20Testing%20An%20AJAX%20Search%20Plugin).

## PHPUnit + WP-CLI command
To get PHPUnit working in Wordpress, you will need to install PHPUnit, WP-CLI and and run a WP-CLI command which gets PHPUnit configured to work with Wordpress. These steps have already been taken for this example plugin.

### Setup

Assuming you have a plugin in your plugins directory, the following proceedure may be used to get started with PHPUnit:

1. **Install PHPUnit** using [composer](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx) into your plugin directory. cd to your plugin's directory and run:  `composer require --dev phpunit/phpunit ^6.4` Double check for an update to this command [here](https://phpunit.de/manual/current/en/installation.html#installation.composer).
2. **Run a WP-CLI command** to install/configure files to get PHPUnit and Wordpress working together. cd to your Wordpress install's root folder and run: `wp scaffold plugin-tests [name-of-plugin]` This will create several new files and folders in your plugin's folder.
3. **Run shell script.** This shell script will create a required database and a test copy of Wordpress itself that will be used by PHPUnit. In your plugin's folder, this script is found in bin/install-wp-tests.sh. Run: `bash bin/install-wp-tests.sh a_database_name root mysql localhost latest` The arguments following the script filename are: the desired name of the new database, your mysql username, mysql password, server name and 'latest', which tells the script to reference the latest version of Wordpress.
4. **Running tests.** From within your plugin's directy run `vendor/bin/phpunit` Assuming you've written the tests correctly and everything else is happy, you'll see the results of your tests displayed in your terminal.

## Other requirements to get PHPUnit tests working.
Test classes must be placed into PHP files within the "tests" folder found in your plugin's folder. By default, these files *must* be prefixed with "test-". Also, the methods you create also must start with the word "test". These defaults can be set in the phpunit.xml.dist file, which was added to your plugin's folder.

### Your Plugin Depends on Other Plugins
If this is the case, technically you've moved from unit testing to *Integration* testing. In any case, you'll need to tell PHPUnit to include that dependency into the testing environment. This is done by editing the `test/bootstrap.php` file that was added when you ran the WP-CLI scaffold command. Note the ‘_manually_load_plugin()’ function. If you need to load additional plugins when you run your tests just add them to the list and they be autoloaded when you run your tests.

```PHP
/**
 * Manually load the plugin being tested.
 */
function _manually_load_plugin() {
    require dirname( dirname( __FILE__ ) ) . '/your-plugin.php';
    require dirname( dirname( dirname( __FILE__ ) ) ) . '/advanced-custom-fields-pro/acf.php';
}
```

Now you can use methods from Advanced Custom Files in your plugin while testing without issue.

## Notes:
- Running that install-wp-tests.sh script actually downloads and installs an entire copy of Wordpress into /tmp/wordpress. This is not in your plugin folder, but the root of your hd. Also, a special version of wp-config.php is installed into /tmp/wordpress-test-lib/wp-test-config.php. You may need to edit this file if the credentials of your mysql testing db change for some reason.

### Additional information on PHP Unit tests can be found at these links:
- https://neliosoftware.com/blog/introduction-to-unit-testing-in-wordpress-phpunit/
- https://neliosoftware.com/blog/introduction-to-unit-testing-in-wordpress-ajax/
- https://pippinsplugins.com/series/unit-tests-wordpress-plugins/ <-better details
- http://richardsweeney.com/testing-integrations/
- http://voceplatforms.com/2014/09/unit-testing-for-wordpress-part-2-writing-unit-tests/

### See also:
- https://github.com/sebastianbergmann/phpunit#installation
- https://phpunit.de/manual/current/en/installation.html#installation.composer
- https://developer.wordpress.org/cli/commands/scaffold/plugin-tests/
- https://make.wordpress.org/core/handbook/testing/automated-testing/
