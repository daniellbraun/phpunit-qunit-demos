<?php
/**
 * The plugin bootstrap file.
 *
 * Plugin Name: Unit Test example - Nelio Post Searcher
 * Description: Unit test example.
 * Version:     1.0.0
 *
 */

/**
 * Adds the search form at the end of a post.
 *
 * @param string $content The content of the current post.
 *
 * @return string the post with the new form.
 *
 * @since  1.0.0
 */
function neliops_add_form( $content ) {

    $input = '<input id="neliops_search" type="text" placeholder="Search&hellip;" />';
    $results = '<div id="neliops_results"></div>';

    if ($_GET['qunit']) {
        $qunit_display = '<div id="qunit"></div>';
    } else {
        $qunit_display = '';
    }

    return $content . $input . $results . $qunit_display;

}// end neliops_add_form

add_filter( 'the_content', 'neliops_add_form' );

/**
 * Enqueues searcher scripts
 */
function neliops_enqueue_scripts() {

    $url = untrailingslashit( plugin_dir_url( __FILE__ ) );

    wp_enqueue_script(
        'neliops-searcher',
        $url . '/searcher.js',
        array( 'jquery', 'underscore' ),
        '1.0.0',
        true
    );

    wp_enqueue_script( 
        'neliops-functions',
        $url . '/functions.js',
        array( 'neliops-searcher' ),
        '1.0.0',
        true
    );

    if ($_GET['qunit']) {

        wp_enqueue_script( 
            'qunit',
            'https://code.jquery.com/qunit/qunit-2.4.1.js',
            array(),
            '2.4.1',
            true
        );

        wp_enqueue_script( 
            'qunit-searcher-tests',
            $url . '/tests/qunit-searcher-tests.js',
            array( 'qunit', 'jquery', 'neliops-searcher' ),
            '1.0.0',
            true
        );

        wp_enqueue_style(
            'qunit-style',
            'https://code.jquery.com/qunit/qunit-2.4.1.css',
            array(),
            '2.4.1'
        );
    }
} // end neliops_enqueue_scripts
add_action( 'wp_enqueue_scripts', 'neliops_enqueue_scripts' );

/**
 * This function returns the posts with a title that contains the query string.
 *
 * @param string $query the search string.
 *
 * @return array List of posts. Each element in the array is an associative
 *               array with the ID, title, and permalink of the matching posts.
 *
 * @since 1.0.0
 */
function neliops_search_posts( $query ) {

    $args = array(
        'post_title__like' => $query,
        'paged' => 1,
        'posts_per_page' => 10,
        'order_by' => 'date',
        'order' => 'desc',
        'post_type' => array( 'post' ),
    );

    add_filter( 'posts_where', 'neliops_add_title_filter_to_wp_query', 10, 2 );
    $query = new WP_Query( $args );
    remove_filter( 'posts_where', 'neliops_add_title_filter_to_wp_query', 10, 2 );

    $result = array();
    while ( $query->have_posts() ) {
        $query->the_post();
        array_push( $result, array(
            'ID' => get_the_ID(),
            'title' => get_the_title(),
            'permalink' => get_the_permalink(),
        ) );
    } // end while
    wp_reset_postdata();

    return $result;
}

/**
 * A filter to search posts based on their title.
 *
 * This function modifies the posts query so that we can search posts based on
 * a term that should appear in their titles.
 *
 * @param string   $where    The where clause, as it's originally defined.
 * @param WP_Query $wp_query The $wp_query object that contains the params used
 *                           to build the where clause.
 *
 * @return string wpdb's where statement.
 *
 * @since  1.0.0
 */
function neliops_add_title_filter_to_wp_query( $where, $wp_query ) {

    global $wpdb;

    if ( $search_term = $wp_query->get( 'post_title__like' ) ) {
        $search_term = $wpdb->esc_like( $search_term );
        $search_term = '\'%' . $search_term . '%\'';
        $where .= ' AND ' . $wpdb->posts . '.post_title LIKE ' . $search_term;
    } // end if

    return $where;
}
add_filter( 'posts_where', 'neliops_add_title_filter_to_wp_query', 10, 2);

/**
 * AJAX callback for searching posts.
 *
 * Expected parameters in the request:
 *  * q: the query string.
 *
 * @since 1.0.0
 */
function neliops_search_posts_ajax_callback() {

    $query = false;
    if ( isset( $_REQUEST['q'] ) ) { // Input var okay.
        $query = sanitize_text_field( wp_unslash( $_REQUEST['q'] ) ); // Input var ok.
    } // end if

    if ( false === $query ) {
        wp_send_json_error( 'Search string can\'t be empty.' ); 
        // If this actually runs, the .draw function in the js file will
        // loop through these 29 characters like it's an array and print
        // <li>undefined</li> 29 times. Duh.
    } // end if

    wp_send_json_success( neliops_search_posts( $query ) );

} // end neliops_search_posts_ajax_callback()
add_action( 'wp_ajax_neliops_search_posts', 'neliops_search_posts_ajax_callback' );
add_action( 'wp_ajax_nopriv_neliops_search_posts', 'neliops_search_posts_ajax_callback' );