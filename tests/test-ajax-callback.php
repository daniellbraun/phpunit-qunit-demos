<?php
/**
 * Class Ajax_Test
 *
 * @package Post_Searcher
 */
class Ajax_Test extends WP_Ajax_UnitTestCase {

    function test_if_there_is_no_query_string_the_callback_should_return_a_non_success_json() {

        try {
            $this->_handleAjax( 'neliops_search_posts' );
            $this->fail( 'Expected exception: WPAjaxDieContinueException' );
        } catch ( WPAjaxDieContinueException $e ) {
            // We expected this, do nothing.
        } // end try

        $response = json_decode( $this->_last_response, true );
        $this->assertFalse( $response['success'] );

    } // test_if_there_is_no_query_string_the_callback_should_return_a_non_success_json

    function test_if_there_is_a_query_string_the_callback_should_return_a_success_json_with_an_array() {

        try {
            $_POST['q'] = 'Search string';
            $this->_handleAjax( 'neliops_search_posts' );
            $this->fail( 'Expected exception: WPAjaxDieContinueException' );
        } catch ( WPAjaxDieContinueException $e ) {
            // We expected this, do nothing.
        } // end try

        $response = json_decode( $this->_last_response, true );
        $this->assertTrue( $response['success'] );
        $this->assertInternalType( 'array', $response['data'] );

    } // test_if_there_is_a_query_string_the_callback_should_return_a_success_json_with_an_array
} // end class