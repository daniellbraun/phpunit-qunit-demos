<?php
/**
 * Class Search_Posts_Function_Test
 *
 * @package Post_Searcher
 */
class Search_Posts_Function_Test extends WP_UnitTestCase {

    function test_if_there_are_no_posts_the_function_should_always_return_an_empty_array() {

        $result = neliops_search_posts( '' );
        $this->assertInternalType( 'array', $result );
        $this->assertEquals( 0, count( $result ) );

        $result = neliops_search_posts( 'some title' );
        $this->assertInternalType( 'array', $result );
        $this->assertEquals( 0, count( $result ) );
        
    } // test_if_there_are_no_posts_the_function_should_always_return_an_empty_array

    function test_if_there_is_one_post_whose_title_contains_the_query_string_the_function_should_return_this_post_in_an_array() {
        $p = $this->factory->post->create( array(
            'post_title' => 'Some title here',
        ) );

        $result = neliops_search_posts( 'Some title here' );
        $this->assertInternalType( 'array', $result );
        $this->assertEquals( 1, count( $result ) );

        $result = neliops_search_posts( 'title' );
        $this->assertInternalType( 'array', $result );
        $this->assertEquals( 1, count( $result ) );

    } // test_if_there_is_one_post_whose_title_contains_the_query_string_the_function_should_return_this_post_in_an_array

    function test_if_there_are_multiple_posts_whose_titles_match_the_search_criteria_the_function_shoould_include_them_all() {
        $this->factory->post->create( array(
            'post_title' => 'First title here',
        ) );
        $this->factory->post->create( array(
            'post_title' => 'Second title here',
        ) );
        $this->factory->post->create( array(
            'post_title' => 'Third title here',
        ) );

        $result = neliops_search_posts( 'title here' );
        $this->assertEquals( 3, count( $result ) );
    } // test_if_there_are_multiple_posts_whose_titles_match_the_search_criteria_the_function_shoould_include_them_all

    function test_the_function_should_not_return_posts_that_dont_match_the_search_criteria() {
        $match = $this->factory->post->create( array(
            'post_title' => 'Valid Title',
        ) );

        $miss = $this->factory->post->create( array(
            'post_title' => 'Something Completely Different',
        ) );

        $result = neliops_search_posts( 'Valid Title' );
        $this->assertEquals( 1, count( $result ) );

        $found_post = $result[0];
        $this->assertEquals( $match, $found_post['ID'] );
        $this->assertNotEquals( $miss, $found_post['ID'] );

    } // test_the_function_should_not_return_posts_that_dont_match_the_search_criteria
}
