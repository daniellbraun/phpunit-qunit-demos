<?php
/**
 * Class Search_Form_Test
 *
 * @package Post_Searcher
 */
class Search_Form_Test extends WP_UnitTestCase {

  function testform_should_appear_at_the_end_of_the_content() {
    
    $pid = $this->factory->post->create( array(
      'post_title'   => 'A Title',
      'post_content' => '<p>Some content.</p>'
    ) );
    
    $post = get_post( $pid );
    $content = apply_filters( 'the_content', $post->post_content );
    $this->assertContains( 'neliops_search', $content );
    $this->assertContains( 'neliops_results', $content );
  
  } // end test_form_should_appear_at_the_end_of_the_content()

} // end class