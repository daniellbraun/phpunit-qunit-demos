QUnit.module('Searcher class', {}, function () {

    QUnit.module('Method "draw"', {}, function () {

        QUnit.module('when the results are empty', {}, function () {

            QUnit.test('should tell the user no posts match the criteria.', function (assert) {

                var $input = jQuery('<input type="text" />');
                var $result = jQuery('<div></div>');
                var searcher = new Searcher($input, $result);

                searcher.draw([]);
                assert.equal('No posts match the search criteria.', $result.text());

            });

        });

        QUnit.module('when the results are not empty', {}, function () {

            QUnit.test('should show all returned posts in a list.', function (assert) {

                var $input = jQuery('<input type="text" />');
                var $result = jQuery('<div></div>');
                var searcher = new Searcher($input, $result);

                var posts = [
                    { ID: 1, title: 'Title 1', permalink: '#' },
                    { ID: 2, title: 'Title 2', permalink: '#' },
                    { ID: 3, title: 'Title 3', permalink: '#' }
                ];
                searcher.draw(posts);
                assert.equal($result.find('ul li').length, posts.length);

            });

        });

    });

});